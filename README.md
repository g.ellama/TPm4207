Durant cette première séance de TP j'ai tout d'abord du réinstaller entièrement Arduino IDE car celui-ci avait planté.

Après une insatallation rapide j'ai pu commencer le TP par la première session, le but de celle-ci est, si j'ai bien compris, est de réussir à localiser la carte arduino, pour celà j'ai donc tout simplement utilisé le programme LGPS qui se trouve dans les exemples ainsi que le module BGPS qui est dans la boîte de la carte arduino, j'ai ainsi pu localiser la carte arduino.

Ensuite j'ai attaqué la session 3 et ai abordé le sujet de la manière suivante, afin de pouvoir obtenir la visualisation de la localisation GPS de la carte arduino j'ai utilisé le module GPS et WIFI afin de pouvoir localiser la carte et créer un serveur web qui collecterer ces données et normalement aurait permis de visualiser directement en ligne la localisation de la carte arduino.
Cependant je n'ai pas réussis à céer un serveur web qui collecterai les données, j'ai cependant réussis à créer et à me connecter à un serveur web quelconque.

Durant cette deuxième séance de TP j'ai commencé par travailler sur le module sonore et j'ai réussi à faire jouer la musique de mario bross.
Ensuite j'ai téléchargé la librairie pour utiliser l'écran LCD et j'ai commencer par changer celui ci de couleur, ensuite j'ai réussis à faire afficher un message dessus.
Mon objectif final étant d'afficher l'état des différents modules sur l'écran LCD afin de savoir s'il sont connecté ou non, d'afficher également l'adresse IP de la carte quand le module WIFI est connecté.
Le but de l'utilisation de l'écran LCD serait de pouvoir afficher les information relatives aux différents port de la carte arduino, cependant je pense que créer un serveur qui donnerai ces même informations serait plus lisible et plus élaboré.
